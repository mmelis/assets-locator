# README #

### AssetLocator allows to use Symfony locator in css and js files 

#### Carefull, not compatible with css rewrite if not called before

This bundle provide symfony locator in js and css files.

To use, add the package to your composer.json

Enable it on the AppKernel.php file:

> new TamTam\Assets\LocatorBundle\TamTamAssetsLocatorBundle()

#### Currently: tamtam/assetic-bust is required (at least a service called tamtam_assets_busting.handler), This dependency will be remove as soon as we find a way to parametrize a service
