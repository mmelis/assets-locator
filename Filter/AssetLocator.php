<?php

namespace TamTam\Assets\LocatorBundle\Filter;

use Assetic\Asset\AssetInterface;
use Assetic\Filter\FilterInterface;
use Symfony\Component\HttpKernel\Kernel;

/**
 * Manage the rewrite of asset names in css (and maybe in js after)
 *
 * Class AssetBustingFilter
 */
class AssetLocator implements FilterInterface
{
    /** @var Kernel The framework kernel.  */
    private $kernel;
    /** @var service If the busting cache is used. */
    private $bustingService;

    /**
     * AssetLocatorFilter constructor.
     * Add needed dependencies.
     *
     * @param Kernel $kernel The symfony kernel.
     */
    public function __construct(Kernel $kernel, $bustingService)
    {
        $this->kernel = $kernel;
        $this->bustingService = $bustingService;
    }

    public function filterDump(AssetInterface $asset)
    {
        $asset->setContent(self::manageAssets($asset->getContent(), $this->kernel, $this->bustingService));
    }

    public static function manageAssets($content, Kernel $kernel, $bustingService)
    {
        preg_match_all(
            '/@([a-zA-Z0-9]+)Bundle\/Resources\/public([a-zA-Z0-9\/\-_.]+)(\|[a-zA-Z0-9\/\-_.|]+)?/',
            $content,
            $matches
        );
        $generated = array();
        $env = $kernel->getEnvironment();
        $busting = $env !== 'dev' && $bustingService;
        foreach (array_unique($matches[0]) as $key) {
            $replaceKey = $key;
            if (isset($generated[$key])) {
                continue;
            }
            $folder = $env === 'dev'? '/bundles': '';
            $pos = strpos($key, '|');
            if (
                $env === 'dev' &&
                $pos
            ) {
                $key = substr($key, 0, $pos);
                $pos = false;
            }
            if (!$pos) {
                $fn = function ($matches) use ($folder, $busting, $env) {
                    if (!$busting) {
                        $folder .= '/' . strtolower($matches[1]);
                    }
                    return $folder . $matches[2];
                };
                $locator = $key;
                $url = preg_replace_callback('/^@([a-zA-Z0-9]+)Bundle\/Resources\/public(.+)/', $fn, $locator);
            } else {
                list($locator, $url) = explode('|', $key);
            }

            if ($env === 'prod') {
                $path = $kernel->locateResource($locator);

                $webPath = $kernel->getRootDir() . '/../web' . $url;
                if ($bustingService) {
                    $url = $bustingService->generateUrlFromPath($path, $url);
                }
                $dir = dirname($webPath);
                if (!is_dir($dir)) {
                    mkdir($dir, 0770, true);
                }

                copy($path, $webPath);
            }

            $generated[$replaceKey] = $url;
        }

        return str_replace(array_keys($generated), array_values($generated), $content);
    }

    /**
     * Filters an asset after it has been loaded.
     *
     * @param AssetInterface $asset An asset
     */
    public function filterLoad(AssetInterface $asset)
    {
    }
}
