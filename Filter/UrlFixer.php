<?php

namespace TamTam\Assets\LocatorBundle\Filter;

use Assetic\Asset\AssetInterface;
use Assetic\Filter\FilterInterface;
use Symfony\Component\HttpKernel\Kernel;

/**
 * Replace the urls in css
 */
class UrlFixer implements FilterInterface
{
    /** @var array The list of mapped urls */
    private $mapping = [];
    /** @var Kernel The framework kernel.  */
    private $kernel;
    /** @var service If the busting cache is used. */
    private $bustingService;

    public function __construct($container, Kernel $kernel, $bustingService)
    {
        if ($container->hasParameter('tamtam_url_fix')) {
            $this->mapping = $container->getParameter('tamtam_url_fix');
        }
        $this->kernel = $kernel;
        $this->bustingService = $bustingService;
    }

    public function filterDump(AssetInterface $asset)
    {
        if ($this->mapping) {
            $content = $asset->getContent();
            preg_match_all('/url\(([^)]*)\)/', $content, $matches);
            foreach (array_unique($matches[1]) as $path) {
                if (isset($this->mapping[$path])) {
                    $content = str_replace('url(' . $path . ')', 'url(' . $this->mapping[$path] . ')', $content);
                }
            }
            $asset->setContent(AssetLocator::manageAssets($content, $this->kernel, $this->bustingService));
        }
    }

    /**
     * Filters an asset after it has been loaded.
     *
     * @param AssetInterface $asset An asset
     */
    public function filterLoad(AssetInterface $asset)
    {
    }
}
